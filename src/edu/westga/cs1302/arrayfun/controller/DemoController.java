package edu.westga.cs1302.arrayfun.controller;

/**
 * The Class DemoController.
 * 
 * @author CS1302
 */
public class DemoController {

	/**
	 * Demos functionality
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void run() {
		
	}

	private int[] createRandomArray(int size, int minValue, int maxValue) {
	
		return null;
	}
	
	private void displayArray(int[] values) {
		System.out.println("Values in array:");

		for (int currValue : values) {
			System.out.println(currValue);
		}
		
		System.out.println();
	}

}
